/**
 * remark plugin to pull out images from inside paragraphs, since <div>s cannot appear inside <p>s.
 */

const visitParents = require("unist-util-visit-parents");

module.exports = ({ markdownAST }, pluginOptions) => {
  visitParents(markdownAST, "paragraph", (node, ancestors) => {
    const parent = ancestors[ancestors.length - 1];
    if (parent && node.children) {
      const imageChildIndex = node.children.findIndex(
        (child) => child.type === "image",
      );
      if (imageChildIndex !== -1) {
        const imageChild = node.children[imageChildIndex];
        const prevChildren = node.children.slice(0, imageChildIndex);
        const nextChildren = node.children.slice(
          imageChildIndex + 1,
          node.children.length,
        );
        const imageNodeIndex = parent.children.findIndex(
          (child) => child === node,
        );
        parent.children.splice(imageNodeIndex + 1, 0, {
          type: "paragraph",
          children: nextChildren,
        });
        parent.children.splice(imageNodeIndex, 0, {
          type: "paragraph",
          children: prevChildren,
        });
        Object.assign(node, imageChild, { children: undefined });
      }
    }
  });
};
