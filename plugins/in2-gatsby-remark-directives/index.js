const visit = require("unist-util-visit");
const mdToString = require("mdast-util-to-string");
const remarkDirective = require("remark-directive");

// treeview recursion to build tree data
function recurse(node, parent) {
  if (node?.children?.length === 0) {
    return null;
  }
  const text = mdToString(node.children[0]);
  const [, icon, name] = text.match(/\{([\w-]+)\}.+\[(.+)\]/) || [];

  const id = parent ? `${parent}.${name}` : name;
  const sublist =
    node.children[1]?.type === "list" ? node.children[1].children : [];

  return {
    icon,
    name,
    id,
    children: sublist.map((nd) => recurse(nd, id)).filter((a) => a),
  };
}

module.exports = ({ markdownAST }, pluginOptions) => {
  visit(
    markdownAST,
    ["textDirective", "leafDirective", "containerDirective"],
    (node) => {
      const data = node.data || (node.data = {});
      if (["success", "info", "warning", "danger"].includes(node.name)) {
        const hast = {
          type: "element",
          tagName: "admonition",
          properties: {
            className: `admonition-${node.name}`,
          },
        };
        data.hName = hast.tagName;
        data.hProperties = hast.properties;

        if (node.children[0]?.data?.directiveLabel) {
          Object.assign(node.children[0], {
            type: "heading",
            depth: 5,
          });
        }
      } else if (node.name === "treeview") {
        if (node.children[0]?.type !== "list") {
          throw new Error("First child of treeview needs to be a list.");
        }
        data.hName = node.name;
        data.hProperties = node.attributes;
        data.hProperties.tree = JSON.stringify(
          node.children[0].children.map(recurse).filter((a) => a),
        );
      } else {
        data.hName = node.name;
        data.hProperties = node.attributes;
      }
    },
  );
};

module.exports.setParserPlugins = () => [remarkDirective];
