import React from "react";
import Root from "./src/core/Root";

export const wrapRootElement = ({ element }) => {
  return <Root>{element}</Root>;
};
