// sanity.cli.js
import { defineCliConfig } from "sanity/cli";

export default defineCliConfig({
  api: {
    projectId: "9v6fntw9",
    dataset: "production",
  },
});
