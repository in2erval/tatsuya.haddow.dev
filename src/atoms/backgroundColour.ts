import { atom } from "recoil";

export default atom({
  key: "bg-colour",
  default: "",
});
