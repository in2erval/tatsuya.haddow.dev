import type {
  EnsuredQueryKey,
  QueryFunctionContext,
  QueryKey,
} from "react-query";
import HttpError from "./HttpError";

export default <TQueryKey extends QueryKey>(
  url: string | ((queryKey: EnsuredQueryKey<QueryKey>) => string),
  options: RequestInit = {},
) => {
  return async ({ queryKey }: QueryFunctionContext<TQueryKey>) => {
    const resolvedUrl = typeof url === "function" ? url(queryKey) : url;
    return fetch(resolvedUrl, options).then((response) => {
      if (!response.ok) {
        throw new HttpError(response);
      }
    });
  };
};
