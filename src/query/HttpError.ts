export default class HttpError extends Error {
  constructor(response?: Response) {
    super(
      `Fetch failed: ${
        response !== undefined ? `HTTP ${response.status}` : "Unexpected error."
      }`,
    );
  }
}
