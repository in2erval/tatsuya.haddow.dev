import "@emotion/react";

declare module "@emotion/react" {
  export interface Theme {
    isLight: boolean;
    main: {
      logo: string;
      toMain: string;
      text: string;
      background: string;
      gradient1: string;
      gradient2: string;
      nav: string;
    };
    list: {
      border: string;
      shadow: string;
      langEn: string;
      langJa: string;
      project: string;
      article: string;
      search: string;
    };
    article: {
      heading: string;
      emphasis: string;
      caption: string;
      quoteBorder: string;
      quoteIcon: string;
      admInfoBase: string;
      admInfoText: string;
      admSuccessBase: string;
      admSuccessText: string;
      admWarningBase: string;
      admWarningText: string;
      admDangerBase: string;
      admDangerText: string;
    };
    code: {
      codeBackground: string;
      codeDefault: string;
      codeSelection: string;
      codeSelectionBackground: string;
      codeAnnotation: string;
      codeAtRule: string;
      codeAttrName: string;
      codeAttrValue: string;
      codeBold: string;
      codeBoolean: string;
      codeBuiltin: string;
      codeCData: string;
      codeChar: string;
      codeClass: string;
      codeComment: string;
      codeConstant: string;
      codeDeleted: string;
      codeDelimiter: string;
      codeDoctype: string;
      codeEntity: string;
      codeFunction: string;
      codeImportant: string;
      codeInserted: string;
      codeInterpolation: string;
      codeItalic: string;
      codeKeyword: string;
      codeNumber: string;
      codeOperator: string;
      codeParameter: string;
      codeProlog: string;
      codeProperty: string;
      codePunctuation: string;
      codeRegex: string;
      codeSelector: string;
      codeString: string;
      codeSymbol: string;
      codeTag: string;
      codeUrl: string;
      codeVariable: string;
    };
  }
}
