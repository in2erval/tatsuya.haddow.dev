import dayjs from "dayjs";
import "dayjs/locale/en-gb";
import "dayjs/locale/ja";

import utc from "dayjs/plugin/utc";
import localisedFormat from "dayjs/plugin/localizedFormat";

dayjs.extend(utc);
dayjs.extend(localisedFormat);
dayjs.locale("en-gb");

export default dayjs;
