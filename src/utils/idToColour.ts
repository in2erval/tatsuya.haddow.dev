import sha1 from "sha1";

export default (id: string): string => {
  return "#" + sha1(id).slice(0, 6);
};
