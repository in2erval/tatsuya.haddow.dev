import styled from "@emotion/styled";
import { graphql } from "gatsby";
import type { PageProps } from "gatsby";
import dayjs from "dayjs";

import ArticleCard from "../../components/ArticleCard";
import breakpoints from "../../utils/breakpoints";
import useWindowSize from "../../utils/useWindowSize";
import i18n from "../../utils/i18n";

const ArticlesListPageStyles = styled.div`
  margin: 1rem;

  .article-list {
    max-width: ${breakpoints.values.lg}px;
    margin: auto;
    margin-bottom: 40px;

    a {
      text-decoration: none;
      color: inherit;
      &:visited,
      &:active,
      &:hover {
        color: inherit;
      }
    }

    .article-list-entry {
      display: flex;
      flex-direction: row;

      .article-list-year {
        width: 15%;
        min-width: 3rem;
        margin-right: 10px;

        p {
          width: 100%;
          margin: 0;
          border-top: 1px solid ${(props) => props.theme.main.text};
        }
      }

      .article-card {
        flex-grow: 1;
        margin-bottom: 24px;
      }
    }
  }
`;

type DataProps = {
  allSanityArticle: {
    nodes: {
      _createdAt: string;
      _updatedAt: string;
      _id: string;
      article_id: {
        current: string;
      };
      title: string;
      subtitle: string;
      image: string;
      lang: string;
      tags: {
        tag_id: {
          current: string;
        };
        label_en: string;
        label_ja: string;
      }[];
    }[];
  };
};

export default function ArticlesPage(props: PageProps<DataProps>) {
  const { width: windowWidth } = useWindowSize();
  const articles = props.data.allSanityArticle.nodes || [];
  const shouldDisplayYear = (index: number) =>
    !articles[index - 1] ||
    dayjs(articles[index - 1]._createdAt).year() !==
      dayjs(articles[index]._createdAt).year();

  const size = windowWidth > 850 ? "full" : "compact";

  return (
    <ArticlesListPageStyles>
      <div className="article-list">
        <h1>{i18n("articles.title")}</h1>
        {articles.map((article, index) => (
          <div key={article._id} className="article-list-entry">
            <div className="article-list-year">
              {shouldDisplayYear(index) && (
                <p>{dayjs(article._createdAt).year()}</p>
              )}
            </div>
            <ArticleCard article={article} cardSize={size} />
          </div>
        ))}
      </div>
    </ArticlesListPageStyles>
  );
}

export const query = graphql`
  query {
    allSanityArticle(sort: { order: DESC, fields: [_createdAt] }) {
      nodes {
        _createdAt
        _updatedAt
        _id
        article_id {
          current
        }
        title
        subtitle
        image
        lang
        tags {
          tag_id {
            current
          }
          label_en
          label_ja
        }
      }
    }
  }
`;

export { default as Head } from "../../core/Head";
