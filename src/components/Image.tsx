import React from "react";
import { useQuery } from "react-query";
import styled from "@emotion/styled";

import request from "../query/request";
import Spinner from "./Spinner";

// Styles

const sizeMap = {
  sd: {
    height: "50vh",
  },
  wd: {
    height: "20vh",
  },
  sm: {
    height: "20vh",
  },
};

type ImageStyleProps = {
  size?: keyof typeof sizeMap;
  fit?: "cover" | "contain";
};

const ImageStyles = styled.figure<ImageStyleProps>`
  width: 100%;
  margin: 0;
  padding: 0;
  position: relative;

  display: flex;
  flex-direction: column;
  align-items: center;

  .invisible {
    opacity: 0;
  }

  div {
    display: flex;
    width: 100%;
    height: ${({ size = "sd" }) => sizeMap[size].height};
    min-height: 400px;
    max-height: 800px;

    overflow-x: auto;

    & > * {
      transition-duration: 1000ms;
    }

    img {
      ${({ fit = "contain" }) => (fit === "cover" ? "width: 100%;" : "")}
      height: 100%;
      margin: auto;
      object-fit: ${({ fit = "contain" }) => fit};
    }
  }

  svg {
    position: absolute;
    top: calc(50% - 2rem);
  }

  figcaption {
    font-style: italic;
  }
`;

// Component

type ImageProps = {
  src: string;
  alt?: string;
  title?: string;
  fit?: "cover" | "contain";
};

function Image(props: ImageProps & React.HTMLProps<HTMLAnchorElement>) {
  const { src, alt, title, fit = "contain", ...rest } = props;
  const size = src.match(
    /\/[A-Za-z0-9_-]+(?:\.(sd|sm|wd))?\./,
  )?.[1] as keyof typeof sizeMap;
  const imageQuery = useQuery(props.src, request(props.src, { method: "GET" }));

  return (
    <ImageStyles {...rest} size={size} fit={fit}>
      <div className="image-container">
        <img
          className={`${imageQuery.isLoading ? "invisible" : ""}`}
          src={src}
          alt={alt || "No description available"}
        />
      </div>
      <Spinner
        className={`${!imageQuery.isLoading ? "invisible" : ""}`}
        role="presentation"
      />
      {title && <figcaption>{title}</figcaption>}
    </ImageStyles>
  );
}

export default Image;
