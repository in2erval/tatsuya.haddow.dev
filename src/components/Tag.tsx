import React from "react";
import styled from "@emotion/styled";
import idToColour from "../utils/idToColour";
import isLight from "../utils/isLight";

type TagProps = {
  id: string;
  label: string;
};

const TagStyles = styled.span`
  display: inline-block;
  padding: 0 10px;
  border-radius: 20px;
  color: ${(props) => (isLight(props.color) ? "black" : "white")};
  background-color: ${(props) => props.color};
`;

function Tag(props: TagProps & React.HTMLProps<HTMLAnchorElement>) {
  return (
    <TagStyles className="tag" color={idToColour(props.id)}>
      {props.label}
    </TagStyles>
  );
}

export default Tag;
