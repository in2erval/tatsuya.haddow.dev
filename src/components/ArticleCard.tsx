import React from "react";
import styled from "@emotion/styled";
import { useRecoilState } from "recoil";
import AniLink from "gatsby-plugin-transition-link/AniLink";

import Image from "./Image";
import Tag from "./Tag";
import dayjs from "../utils/dayjs";
import isLight from "../utils/isLight";
import i18n from "../utils/i18n";
import langAtom from "../atoms/lang";

type ArticleCardProps = {
  article: {
    _createdAt: string;
    _updatedAt: string;
    article_id: {
      current: string;
    };
    title: string;
    subtitle: string;
    image: string;
    lang: string;
    tags: {
      tag_id: {
        current: string;
      };
      label_en: string;
      label_ja: string;
    }[];
  };
  cardSize: "full" | "compact";
};

const ArticleCardStyles = styled.div<ArticleCardProps>`
  position: relative;
  transition: all 250ms;

  &:hover,
  &:focus {
    transform: scale(1.02);
  }

  .article-card-link {
    position: absolute;
    top: 0px;
    bottom: 0px;
    right: 0px;
    left: 0px;
    z-index: 10;
  }

  .article-card-content {
    display: grid;

    grid-template-columns: 40% auto;
    grid-template-areas:
      "image title"
      "image details"
      "image subtitle"
      "image .";
    grid-column-gap: 10px;

    @media screen and (max-width: 850px) {
      grid-template-columns: auto;
      grid-template-areas:
        "image"
        "title"
        "details"
        "subtitle";

      & > *:not(figure) {
        padding: 0 10px;
      }
    }

    position: relative;
    border-radius: 20px;
    box-shadow: 0px 12px 12px -12px ${(props) => props.theme.list.shadow},
      0px 0px 12px -6px ${(props) => props.theme.list.shadow};

    overflow: hidden;

    .article-card-title {
      grid-area: title;
      font-size: 2rem;
      margin: 1rem 0;
    }

    .article-card-subtitle {
      grid-area: subtitle;
    }

    .article-card-lang {
      position: absolute;
      right: 0;
      padding: 0 10px;
      border-bottom-left-radius: 10px;
      font-weight: bold;
      z-index: 5;

      &.en {
        color: ${(props) =>
          isLight(props.theme.list.langEn) ? "black" : "white"};
        background: ${(props) => props.theme.list.langEn};
      }
      &.ja {
        color: ${(props) =>
          isLight(props.theme.list.langJa) ? "black" : "white"};
        background: ${(props) => props.theme.list.langJa};
      }
    }

    .article-card-image {
      grid-area: image;

      .image-container {
        height: 100%;
        min-height: 200px;
      }
    }

    .article-card-details {
      grid-area: details;

      span {
        margin-right: 6px;
        white-space: nowrap;
      }

      .article-card-dates {
        display: flex;
        flex-wrap: wrap;
      }

      .article-card-tags {
        display: flex;
        flex-wrap: wrap;

        margin-top: 1rem;

        a {
          margin-right: 6px;
          margin-bottom: 6px;
          position: relative;
          z-index: 10;
        }
      }

      .meta-icon {
        width: 1.5rem;
        height: 1.5rem;
        vertical-align: text-bottom;
      }
    }
  }
`;

function ArticleCard(
  props: ArticleCardProps & React.HTMLProps<HTMLDivElement>,
) {
  const {
    cardSize = "full",
    article: {
      _createdAt: createdAt,
      _updatedAt: updatedAt,
      article_id,
      title,
      subtitle,
      image,
      lang = "en",
      tags,
    },
    ...rest
  } = props;
  const locale = lang === "en" ? "en-gb" : lang;
  const [siteLang] = useRecoilState(langAtom);

  return (
    <ArticleCardStyles {...rest} className={`article-card ${cardSize}`}>
      <AniLink
        fade
        to={`/articles/${article_id?.current}`}
        className="article-card-link"
        aria-label={title}
      />
      <div className={`article-card-content ${cardSize}`}>
        <h3 className="article-card-title">{title}</h3>
        <h4 className="article-card-subtitle">{subtitle}</h4>
        <span className={`article-card-lang ${lang}`}>
          {lang.toUpperCase()}
        </span>
        <Image className="article-card-image" src={image || ""} fit="cover" />
        <div className="article-card-details">
          <div className="article-card-dates">
            <span className="article-card-created-at">
              <svg className="meta-icon" viewBox="0 0 24 24">
                <path
                  fill="currentColor"
                  d="M19 3H18V1H16V3H8V1H6V3H5C3.9 3 3 3.89 3 5V19C3 20.11 3.9 21 5 21H19C20.11 21 21 20.11 21 19V5C21 3.89 20.11 3 19 3M19 19H5V9H19V19M19 7H5V5H19M7 11H12V16H7"
                />
              </svg>
              {i18n("articles.created")}:{" "}
              {dayjs(createdAt).locale(locale).format("LL")}
            </span>
            <span className="article-card-updated-at">
              <svg className="meta-icon" viewBox="0 0 24 24">
                <path
                  fill="currentColor"
                  d="M14.06,9L15,9.94L5.92,19H5V18.08L14.06,9M17.66,3C17.41,3 17.15,3.1 16.96,3.29L15.13,5.12L18.88,8.87L20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18.17,3.09 17.92,3 17.66,3M14.06,6.19L3,17.25V21H6.75L17.81,9.94L14.06,6.19Z"
                />
              </svg>
              {i18n("common.updated")}:{" "}
              {dayjs(updatedAt).locale(locale).format("LL")}
            </span>
          </div>
          <div className="article-card-tags">
            {tags?.map((tag) => (
              <AniLink
                fade
                to={`/tags/${tag.tag_id.current}`}
                key={tag.tag_id.current}
              >
                <Tag
                  className="article-card-tag"
                  label={siteLang === "ja" ? tag.label_ja : tag.label_en}
                  id={tag.tag_id.current}
                />
              </AniLink>
            ))}
          </div>
        </div>
      </div>
    </ArticleCardStyles>
  );
}

export default ArticleCard;
