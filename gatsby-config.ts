import type { GatsbyConfig } from "gatsby";

const config: GatsbyConfig = {
  siteMetadata: {
    title: `tatsuya.haddow.dev`,
    siteUrl: `https://tatsuya.haddow.dev`,
  },
  plugins: [
    {
      resolve: "gatsby-source-sanity",
      options: {
        projectId: "9v6fntw9",
        dataset: "production",
      },
    },
    {
      resolve: "gatsby-plugin-transition-link",
      options: {
        layout: `${__dirname}/src/layouts/index.tsx`,
      },
    },
    "gatsby-plugin-emotion",
    "gatsby-plugin-image",
    {
      resolve: "gatsby-transformer-remark",
      options: {
        plugins: [
          {
            resolve: "gatsby-remark-prismjs",
            options: {},
          },
          {
            resolve: "gatsby-remark-katex",
            options: {},
          },
          {
            resolve: "in2-gatsby-remark-directives",
            options: {},
          },
          {
            resolve: "in2-gatsby-remark-dom-nesting",
            options: {},
          },
        ],
      },
    },
  ],
};

export default config;
