exports.createSchemaCustomization = ({ actions, schema }) => {
  const { createTypes } = actions;

  const typeDefs = [
    // Create a discriminated union of content type
    `union Content = SanityProject | SanityArticle`,
    // Create a "related_content" field on the root type of "SanityTag"
    `type SanityTag implements Node { 
      related_content: [Content]  
    }`,

    schema.buildObjectType({
      name: "SanityTag",
      fields: {
        related_content: {
          type: ["Content"],
          resolve: async (source, args, context, info) => {
            const projects = await context.nodeModel.findAll({
              type: "SanityProject",
              query: {
                filter: {
                  tags: {
                    elemMatch: {
                      tag_id: {
                        current: { eq: source.tag_id.current },
                      },
                    },
                  },
                },
              },
            });
            const articles = await context.nodeModel.findAll({
              type: "SanityArticle",
              query: {
                filter: {
                  tags: {
                    elemMatch: {
                      tag_id: {
                        current: { eq: source.tag_id.current },
                      },
                    },
                  },
                },
              },
            });

            return [...projects.entries, ...articles.entries];
          },
        },
      },
    }),
  ];
  createTypes(typeDefs);
};

exports.onCreateNode = async (create) => {
  const { node, actions, createNodeId, createContentDigest } = create;

  // Create a markdown child node for each article
  if (node.internal.type === "SanityArticle") {
    const markdownNode = {
      id: createNodeId(`${node.id} >>> MarkdownRemark`),
      children: [],
      parent: node.id,
      internal: {
        content: node.content,
        type: "MarkdownRemark",
        contentDigest: "",
      },
      title: node.title,
      rawMarkdownBody: node.content,
    };
    markdownNode.internal.contentDigest = createContentDigest(markdownNode);

    actions.createNode(markdownNode);
    actions.createParentChildLink({ parent: node, child: markdownNode });

    return markdownNode;
  }
};
