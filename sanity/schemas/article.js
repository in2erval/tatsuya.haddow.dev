export default {
  title: "Article",
  name: "article",
  type: "document",
  fields: [
    {
      title: "Article ID",
      name: "article_id",
      type: "slug",
      options: {
        source: "title",
      },
    },
    {
      title: "Title",
      name: "title",
      type: "string",
    },
    {
      title: "Subtitle",
      name: "subtitle",
      type: "string",
    },
    {
      title: "Image",
      name: "image",
      type: "url",
      validation: (Rule) =>
        Rule.uri({
          allowRelative: true,
        }),
    },
    {
      title: "Language",
      name: "lang",
      type: "string",
      options: {
        list: ["en", "ja"],
      },
    },
    {
      title: "Tags",
      name: "tags",
      type: "array",
      of: [{ type: "reference", to: [{ type: "tag" }] }],
    },
    {
      title: "Content",
      name: "content",
      type: "text",
    },
  ],
};
