export default {
  title: "Tag",
  name: "tag",
  type: "document",
  fields: [
    {
      title: "English Label",
      name: "label_en",
      type: "string",
    },
    {
      title: "Japanese Label",
      name: "label_ja",
      type: "string",
    },
    {
      title: "Tag ID",
      name: "tag_id",
      type: "slug",
      options: {
        source: "label_en",
      },
    },
  ],
};
