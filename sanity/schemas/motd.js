export default {
  title: "MOTD",
  name: "motd",
  type: "document",
  fields: [
    {
      title: "Message",
      name: "message",
      type: "string",
    },
    {
      title: "Language",
      name: "lang",
      type: "string",
      options: {
        list: ["en", "ja"],
      },
    },
  ],
};
