export default {
  title: "About Me",
  name: "about",
  type: "document",
  fields: [
    {
      title: "English",
      name: "content_en",
      type: "text",
    },
    {
      title: "Japanese",
      name: "content_ja",
      type: "text",
    },
  ],
};
