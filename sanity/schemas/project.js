export default {
  title: "Project",
  name: "project",
  type: "document",
  fields: [
    {
      title: "Project ID",
      name: "project_id",
      type: "slug",
      options: {
        source: "name",
      },
    },
    {
      title: "Name",
      name: "name",
      type: "string",
    },
    {
      title: "Descriptio (English)",
      name: "description_en",
      type: "text",
    },
    {
      title: "Description (Japanese)",
      name: "description_ja",
      type: "text",
    },
    {
      title: "Tags",
      name: "tags",
      type: "array",
      of: [{ type: "reference", to: [{ type: "tag" }] }],
    },
    {
      title: "Icon",
      name: "icon",
      type: "url",
      validation: (Rule) =>
        Rule.uri({
          allowRelative: true,
        }),
    },
    {
      title: "Links",
      name: "links",
      type: "array",
      of: [{ type: "link" }],
    },
    {
      title: "Screenshots",
      name: "screenshots",
      type: "array",
      of: [
        {
          type: "url",
          validation: (Rule) =>
            Rule.uri({
              allowRelative: true,
            }),
        },
      ],
    },
    {
      title: "Embed URL",
      name: "embed",
      type: "url",
    },
    {
      title: "Video URL",
      name: "video",
      type: "url",
    },
  ],
};
