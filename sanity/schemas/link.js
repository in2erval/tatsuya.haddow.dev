export default {
  title: "Link",
  name: "link",
  type: "object",
  fields: [
    {
      title: "Type",
      name: "type",
      type: "string",
      options: {
        list: [
          { title: "Github", value: "github" },
          { title: "Gitlab", value: "gitlab" },
          { title: "Docsite", value: "docs" },
          { title: "Homepage", value: "homepage" },
          { title: "Article", value: "article" },
          { title: "Other", value: "other" },
        ],
      },
    },
    {
      title: "URL",
      name: "url",
      type: "url",
      validation: (Rule) =>
        Rule.uri({
          allowRelative: true,
        }),
    },
  ],
};
