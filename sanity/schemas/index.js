import Article from "./article";
import Link from "./link";
import Project from "./project";
import Tag from "./tag";
import Motd from "./motd";
import About from "./about";

export default [About, Article, Tag, Project, Link, Motd];
